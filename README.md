# ansible-rlab

Ansible tasks for rlab infra

# Requsites
* Passwordless sudo user
* ansible >= 1.8 
* Populated ~/.ssh/config

# Usage

```
ansible-playbook playbooks/example.yml -i hosts.yml -e target=vms
```

Playboks:
  - apt.yml -> updates packages from SO (debian)
  - apk.yml -> update packages from SO (alpine)
  - add-users.sh -> add users to server
  - Pull and restart docker-compose based services
